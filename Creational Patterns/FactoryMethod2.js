function Product() {this.getName=null;}
 
function ConcreteProductA() {
    this.getName=function(){return "ConcreteProductA"};
}
 
function ConcreteProductB() {
    this.getName=function(){return "ConcreteProductB"};
}
 
function Creator() {this.factoryMethod=null;}
 
function ConcreteCreatorA() {
    this.factoryMethod=function(){return new ConcreteProductA();};
}
 
function ConcreteCreatorB() {
    this.factoryMethod=function(){return new ConcreteProductB();};
}

ConcreteProductA.prototype = new Product();
ConcreteProductA.prototype.constructor = ConcreteProductA;

ConcreteProductB.prototype = new Product();
ConcreteProductB.prototype.constructor = ConcreteProductB;

ConcreteCreatorA.prototype = new Creator();
ConcreteCreatorA.prototype.constructor = ConcreteCreatorA;

ConcreteCreatorB.prototype = new Creator();
ConcreteCreatorB.prototype.constructor = ConcreteCreatorB;
 
// An array of creators
var creators = [new ConcreteCreatorA,new ConcreteCreatorB];
// Iterate over creators and create products
for (var i=0; i < creators.length; i++) {
    var product = creators[i].factoryMethod();
    console.log( product.getName() );
}
