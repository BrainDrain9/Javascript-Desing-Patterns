var fromPrototype = function(prototype, object) {
    var newObject = Object.create(prototype);
    for (var prop in object) {
        if (object.hasOwnProperty(prop)) {
            newObject[prop] = object[prop];
        }
    }
  return newObject;
};


// Define our `Ingredients` base object
var Ingredients = {
    createDough: function() {
        return 'generic dough';
    },
    createSauce: function() {
        return 'generic sauce';
    },
    createCrust: function() {
        return 'generic crust';
    }
};

// Extend `Ingredients` with concrete implementations
Ingredients.createChicagoStyle = function() {
    return fromPrototype(Ingredients, {
        createDough: function() {
            return 'thick, heavy dough';
        },
        createSauce: function() {
            return 'rich marinara';
        },
        createCrust: function() {
            return 'deep-dish';
        }
    });
};

Ingredients.createCaliforniaStyle = function() {
    return fromPrototype(Ingredients, {
        createDough: function() {
            return 'light, fluffy dough';
        },
        createSauce: function() {
            return 'tangy red sauce';
        },
        createCrust: function() {
            return 'thin and crispy';
        }
    });
};

// Try it out!
var californiaIngredients = Ingredients.createCaliforniaStyle();
console.log(californiaIngredients.createCrust()); // returns 'thin and crispy';
