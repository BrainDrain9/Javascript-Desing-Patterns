var mySingleton = (function () {
    var instance;
  
    function createInstance() {
        var object = new Object("I am the instance");
        return object;
    }
  
    return {
        getInstance: function () {
            if (!instance) {
                instance = createInstance();
            }
            return instance;
        }
    };
})();
 
// Usage 
    var instance1 = mySingleton.getInstance();
    var instance2 = mySingleton.getInstance();
 
//check instances
    alert("Same instance? " + (instance1 === instance2));  
}
